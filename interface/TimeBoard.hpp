#ifndef TIME_BOARD_H
#define TIME_BOARD_H

#include "interface/StandardIncludes.hpp"
#include "interface/HwManager.hpp"


class TimeBoard : public Board {
// this class implements a dummy board that will be put in the event containing the time it is called
protected:

public:
    TimeBoard();
    int Init() ;
    int Clear();
    int Print();
    inline int BufferClear() { return 0; };
    int Config(BoardConfig*);
    int Read(vector<WORD> &v);
    void SetHandle(int x) {};

    void SetTimeRef();

    struct timeval  begin_of_spill_time_; //synchronised every spill using NTP
    struct timeval  trig_time_; //relative to begin of spill using CLOCK_MONOTONIC_RAW
};

#endif
