#ifndef CAEN_DT5495_H
#define CAEN_DT5495_H

#include "interface/StandardIncludes.hpp"
#include "interface/Board.hpp"
#include "interface/BoardConfig.hpp"



// #define CAEN_DT5495_DATAWIDTH cvD16
/* #define CAEN_DT5495_ADDRESSMODE cvA32_U_DATA

   #define CAEN_DT5495_PATTERNUNIT_MASKA_ADDRESS 0x1010
   #define CAEN_DT5495_PATTERNUNIT_MASKB_ADDRESS 0x1014
   #define CAEN_DT5495_PATTERNUNIT_MASKE_ADDRESS 0x101C
   #define CAEN_DT5495_PATTERNUNIT_MASKF_ADDRESS 0x1020
   #define CAEN_DT5495_PATTERNUNIT_CTRLREG_ADDRESS 0x1018
   #define CAEN_DT5495_PATTERNUNIT_DELAY_ADDRESS 0x1024

   #define CAEN_DT5495_PATTERNUNIT_PATTERNA_ADDRESS 0x103C
   #define CAEN_DT5495_PATTERNUNIT_PATTERNB_ADDRESS 0x1040
   #define CAEN_DT5495_PATTERNUNIT_PATTERNE_ADDRESS 0x1044
   #define CAEN_DT5495_PATTERNUNIT_PATTERNF_ADDRESS 0x1048
   #define CAEN_DT5495_PATTERNUNIT_STATUS_ADDRESS 0x1050

   #define CAEN_DT5495_PATTERNUNIT_MODULERESET_ADDRESS 0x800A

   #define CAEN_DT5495_PATTERNUNIT_VMEFPGA_FWVERSION_ADDRESS 0x800C
   #define CAEN_DT5495_PATTERNUNIT_VMEFPGAFWVERSION_MAJORNUMBER_BITMASK 0xFF
   #define CAEN_DT5495_PATTERNUNIT_VMEFPGAFWVERSION_MINORNUMBER_BITMASK 0xFF

   #define CAEN_DT5495_PATTERNUNIT_USERFPGA_FWVERSION_ADDRESS 0x100C
   #define CAEN_DT5495_PATTERNUNIT_USERFPGAFWVERSION_MAJORNUMBER_BITMASK 0xF
   #define CAEN_DT5495_PATTERNUNIT_USERFPGAFWVERSION_MINORNUMBER_BITMASK 0xF */

/*#define CAEN_DT5495_ENABLED_PORTS_REGISTER 0x1808  // write 8 bits
  #define CAEN_DT5495_CONTROL_PORT_REGISTER 0x1809   // write 4 bits
  #define CAEN_DT5495_ENABLED_BUSY_REGISTER 0x1810   // write 10 bits - used to be 1811
  #define CAEN_DT5495_FULL_DELAY_REGISTER 0x180C     //write 32 bits
  #define CAEN_DT5495_SPS_REGISTER 0x1004  	   //To read from the DT5495 Board
  #define CAEN_DT5495_BUSY_FLAG_REGISTER 0x1818      // write 1 bit  - used to be 1810*/

#define CAEN_DT5495_FLIP_INPUT_REGISTER     0x1808  // 32 bit reg only first 8 used
#define CAEN_DT5495_CONTROL_PORT_REGISTER   0x180C  // 32 bit reg only first 4 used 
#define CAEN_DT5495_ENABLED_PORTS_REGISTER  0x1810  // 32 bit reg only first 8 used
#define CAEN_DT5495_SPS_LAST_SIGNAL         0x1004  // 32 bit reg only first 3 used
#define CAEN_DT5495_INTERNAL_BUSY_MON       0x100C  // only first bit, 1 if internal busy ON, 0 if OFF, read only
#define CAEN_DT5495_CONTROL_DELAY_REGISTER  0x1818  // 32 bit reg
#define CAEN_DT5495_ENABLED_BUSY_REGISTER   0x181C  // 32 bit reg only first 8 used
#define CAEN_DT5495_BUSY_FLAG_REGISTER      0x1820  // 32 bit reg only first 1 used
#define CAEN_DT5495_SELFTRIGGER_REGISTER    0x1824  // 32 bit reg 
#define CAEN_DT5495_SELFTRIGGER_DOWNTIME    0x1828 
#define CAEN_DT5495_SELFTRIGGER_UPTIME      0x182C
  
#define CAEN_DT5495_SPS_GENERATOR_REGISTER  0x1830  // 32 bit reg only first 1 used
                                                    // bit 0: sps signal generator enable/disable
#define CAEN_DT5495_TRIGGER_FLAG            0x1008  // 32 bit reg only first 1 used
                                                    // bit 0: Trigger flag high/low
#define CAEN_DT5495_INTERNAL_BUSY_LENGTH    0x1834  // 32 bit reg only first 16 bits used
						    // Number of clocks while internal busy flag is kept high
#define CAEN_DT5495_INTERNAL_BUSY_RESET     0x1838  // 32 bit reg only first 1 bit used
                                                    // Bit to reset internal busy, move it from 1 to 0 to lower the flag
#define T_0 12.0
#define T_1 10.7
/*
  #define CAEN_DT5495_MASKA_ADDRESS 0x1800
  #define CAEN_DT5495_MASKB_ADDRESS 0x1804
  #define CAEN_DT5495_MASKD_ADDRESS 0x101C
  #define CAEN_DT5495_MASKF_ADDRESS 0x1020

  #define CAEN_DT5495_PATTERNUNIT_VMEFPGAFWVERSION_MAJORNUMBER_BITMASK 0xFF
  #define CAEN_DT5495_PATTERNUNIT_VMEFPGAFWVERSION_MINORNUMBER_BITMASK 0xFF
*/


class CAEN_DT5495: public TriggerBoard, IOControlBoard
{
public:
    typedef enum  {
        ERR_NONE= 0,
        ERR_CONF_NOT_FOUND,
        ERR_OPEN,
        ERR_CONFIG,
        ERR_RESET,
        ERR_READ,
        ERR_DUMMY_LAST,
        ERR_CONF_INVALID,
    } ERROR_CODES;

    uint32_t REGISTERS[11] = {
        CAEN_DT5495_FLIP_INPUT_REGISTER,
        CAEN_DT5495_CONTROL_PORT_REGISTER,
        CAEN_DT5495_ENABLED_PORTS_REGISTER,
        CAEN_DT5495_SPS_LAST_SIGNAL,
        CAEN_DT5495_CONTROL_DELAY_REGISTER,
        CAEN_DT5495_ENABLED_BUSY_REGISTER,
        CAEN_DT5495_BUSY_FLAG_REGISTER,
        CAEN_DT5495_SELFTRIGGER_REGISTER,
        CAEN_DT5495_SPS_GENERATOR_REGISTER,
	    CAEN_DT5495_TRIGGER_FLAG,
	    CAEN_DT5495_INTERNAL_BUSY_LENGTH
    };
  
    map<string, int> portType = {{"TriggerInput", 0}, {"TriggerOutput", 1}, {"Busy", 2}, {"ControlSig", 3}, {"SPS", 4}};
  
    typedef enum DT5495_DAQ_Signals 
    {
        DAQ_CLEAR_BUSY = 0,
        DAQ_TRIG_ACK,
        DAQ_BUSY_ON,
        DAQ_BUSY_OFF,
        DAQ_LAST_SIGNAL,
    } DT5495_DAQ_Signals;

    typedef struct EnabledCh {
        unsigned int id;
        unsigned int port;
        unsigned int gate;
        unsigned int delay;
        unsigned int convertedGate;
        unsigned int convertedDelay;
        string type;
    } EnabledCh;
  
    typedef struct CAEN_DT5495_Config_t {
        string IPAddress;
        string runType;
        unsigned int maskA;
        unsigned int maskB;
        unsigned int maskE;
        unsigned int maskF;
        unsigned int sigDelay;
        unsigned int ctrlPort;
        unsigned int enabledPorts;
        unsigned int enabledBusy;
        unsigned int selfTriggerEnabled;
        unsigned int stDowntime;
        unsigned int stUptime;
	unsigned int internalBusyLength;
        unsigned int spsSimEnabled;
        vector<EnabledCh> Chl;
    } CAEN_DT5495_Config_t;

    CAEN_DT5495(): TriggerBoard(), IOControlBoard(), handle_(-1), trig_count_(-1) { type_="CAEN_DT5495"; };
    virtual int Init();
    virtual int Clear();
    virtual int BufferClear();
    virtual int Print() { return 0; }
    virtual int Config(BoardConfig *bC);
    virtual int Read(vector<WORD> &v);
    virtual void SetHandle(int handle) {handle_=handle;};
    virtual int ClearBusy();
    virtual int SetBusyOff();
    virtual int SetBusyOn();
    virtual bool SignalReceived(CMD_t signal);
    virtual bool TriggerReceived();
    virtual int TriggerAck();
    //virtual int UpdateGDG(int enable, char* port, int portnu, double gate, double delay);
    virtual int UpdateGDG(char* new_gdg_pars);
    virtual int UpdateSelfTrigger(char* new_seftrigger_pars);
    virtual int EnableSelfTrigger();
    //virtual int UpdateSpsSim(char* new_spssim_pars);
    //virtual int EnableSpsSim();
    virtual int SendHwCommand(CMD_t command, bool status) {return 0;};
    inline CAEN_DT5495_Config_t* GetConfiguration() { return &configuration_; };
    int SetTriggerStatus(TRG_t triggerType, TRG_STATUS_t triggerStatus);

    //Write a message to the Board
    int WriteReg (uint32_t reg, uint32_t value, bool closeConnection = false) {
	ostringstream s; s << "[CAEN_DT5495]::[WriteReg] going to write register " << reg << " with value=" << value;
	Log(s.str(),1);

        int status = 0;
        string DT_ip = configuration_.IPAddress;
        char *ip = &DT_ip[0u];
        status = CAEN_PLU_WriteReg(handle_, reg, value);
	s.str(""); s << "[CAEN_DT5495]::[WriteReg] done with exit status " << status;
	Log(s.str(),1);

        //ostringstream s; s << "status after write reg "<<status<<"\n";
        //Log(s.str(),1);
        if (status == -8) { // Invalid handle used: maybe the connection is not open. Doing now
          status = CAEN_PLU_OpenDevice(CAEN_PLU_CONNECT_DIRECT_ETH, ip, 0, 0, &handle_);
          // Repeat write
          status |= CAEN_PLU_WriteReg(handle_, reg, value);
        }
        if (closeConnection) {
          status |= CAEN_PLU_CloseDevice(handle_);
        }
        return status;
    }
  
    //Read a message from the Board
    int ReadReg (uint32_t reg, uint32_t* value, bool closeConnection = false) {
	//ostringstream s; s << "[CAEN_DT5495]::[ReadReg] going to read register:" << reg;
	//Log(s.str(),1);

        int status = 0;
        string DT_ip = configuration_.IPAddress;
        char *ip = &DT_ip[0u];
        status = CAEN_PLU_ReadReg(handle_, reg, value); 
        //ostringstream s; s << "status after read reg "<<status<<"\n";
        //Log(s.str(),1);

	//s.str(""); s << "[CAEN_DT5495]::ReadReg read value:" << *value;
	//Log(s.str(),1);
        if (status == -8) { // Invalid handle used: maybe the connection is not open. Doing now
          status = CAEN_PLU_OpenDevice(CAEN_PLU_CONNECT_DIRECT_ETH, ip, 0, 0, &handle_);
          // Repeat read
          status |= CAEN_PLU_ReadReg(handle_, reg, value);
        }
        if (closeConnection) {
          status |= CAEN_PLU_CloseDevice(handle_);
        }
        return status;
    }

    //Convert the gate and delay times to the parameters needed by the formulas on pg 
    int gdConverter (vector<EnabledCh> &T, int i) {
        if ( T[i].type != "ControlSig" )
        {
            T[i].convertedDelay = round( (T[i].delay - T_0)/T_1 + 1 );
            T[i].convertedGate  = round( T[i].gate/T_1 + 2 ); 
        }
        return 0;
    }

    int fromNsToClockCycles (int i) {
        int temp_value = i/20;
        if (temp_value <= 0) {
            temp_value = 1;
        }
        return temp_value; 
    }
  


private:
    int handle_;
    int trig_count_;
    //int SendSignal(DT5495_DAQ_Signals sig);
    CAEN_DT5495_Config_t configuration_;
    int ParseConfigForChannels (BoardConfig * bC, const xmlNode * node);
    int PrintPortsConfiguration();
    int PrintReg();
    int controlidx = -1;
    int ReadInput(uint32_t& data);
};

#endif
