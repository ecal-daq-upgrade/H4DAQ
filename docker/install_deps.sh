#!/bin/bash

# install dependencies
dnf install -y make root libxml2-devel zeromq zeromq-devel python3-devel

# install the CAEN libraries
cd caen/CAENVMELib-v3.4.0/lib/ && ./install_x64 && cd -
cd caen/CAENComm-v1.6.0/lib/ && ./install_x64 && cd -
cd caen/CAENDigitizer_2.9.1 && ./install_64 && cd -
cd caen/CAEN_PLU-1.2 && ./install_x64 && cd -

