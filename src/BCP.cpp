#ifndef NO_BCP
#include "interface/BCP.hpp"
#include <iostream>
#include <fstream>
#include <set>
#include <ctime>

extern void Log(std::string s, short level); 


BCP::BCP(bool pyFinalize, const std::string &ip, 
	 unsigned serverPort,
	 bool debug,
	 std::string path,
	 std::string roguepath) : Board(), handle_(-1) {


  Log("[BCP] Init Python...", 1);
  type_ = "BCP";
  pypath_ = path;
  roguepath_ = roguepath; 

  pyFinalize_ = pyFinalize;
  if (!Py_IsInitialized())
    setupPython(); 


  debug_ = debug; 

  Log("[BCP] Ready for Initialization...", 1);

}

BCP::~BCP() {

  cleanupPython(); 
}

void BCP::setupPython()
{ 
  Py_Initialize(); 
  try
    {
      if (debug_)
	Log("BCP: Setting up python",1); 
	
      //std::cout << "Importing rogue / other python libs" << std::endl;
      bp::object main = bp::import("__main__"); 
      bp::object global = (main.attr("__dict__")); 

      bp::exec(genPythonImports().c_str(), global, global); 

    }
  catch (boost::python::error_already_set) {
    PyErr_Print();
  }


}

void BCP::cleanupPython()
{


  if (pyFinalize_) {
    //std::cout << "Cleaning up python..." << std::endl; 
    // Don't call Py_Finalize, currently not supported
    //Py_Finalize(); 
  }


}

std::string BCP::genPythonImports() { 

  // Path to python readout files
    boost::filesystem::path base(pypath_);
    boost::filesystem::path rogue(roguepath_); 
    //  boost::filesystem::path base("/home/tanderso/bcp_controller_h4_oct2021"); 
    //  boost::filesystem::path rogue("/home/tanderso/miniconda3/envs/H4DAQ/lib/python3.7/site-packages"); 
  boost::filesystem::path fw = base; 
  boost::filesystem::path surf = base; 
  //current = boost::filesystem::current_path(); 
  fw.append("bcp-readout");
  fw.append("firmware");

  //  current = boost::filesystem::current_path();
  surf.append("bcp-readout");
  surf.append("firmware/surf/python");

  std::string imports = "import os, sys \n"; 
  imports += "sys.path.append('"; 
  imports += surf.native(); 
  imports += "')\n";
  imports += "sys.path.append('" + rogue.native() + "')\n"; 
  imports += "sys.path.append('" + base.native() + "')\n"; 
  imports += "sys.path.append('" + fw.native() + "')\n"; 
  imports += "import lpgbt_functions \n"; 
  //std::cout << imports << std::endl; 
  

  return imports; 

}



int BCP::Init() {

  if (!Py_IsInitialized()) {
    //std::cerr << "Error! BCP::Init called before python initialization!" << std::endl; 
    return -1;
  }
  try {


    
    lpgbtfns_ = bp::import("lpgbt_functions");
    
    Log("Attempting to configure and start BCP.....",1); 
    bp::object bpo = lpgbtfns_.attr("bcpConfigureAndStart")();


    Log("[BCP] Configured and started...", 1);

    bcp_ = bpo; 
    bp::object builtins = bp::import("builtins");
    bcpobjs_["builtins"] = builtins;
    bcpobjs_["Core"] = bcp_.attr("Core");
    bcpobjs_["App"] = bcp_.attr("App");
    bcpobjs_["AppTx"] = bcp_.attr("App").attr("AppTx"); 
    bcpobjs_["SwRx"] = bcp_.attr("SwRx");
    bcpobjs_["BCP"] = bcp_.attr("BCP");
    bcpobjs_["Lpgbt"] = bcp_.attr("BCP").attr("BCP_Lpgbt");
    bcpobjs_["Readout"] = bcp_.attr("BCP").attr("BCP_Readout");
    bcpobjs_["TCDS"] = bcp_.attr("BCP").attr("BCP_TCDS");
    
    setupReadout(); 


    fpgaversion_ = bp::extract<int>(bpo.attr("Core").attr("AxiVersion").attr("FpgaVersion").attr("get")());
    std::cout << std::hex << "FPGA Version: " << fpgaversion_ << std::endl;
    std::cout << std::dec;
  }
  catch (boost::python::error_already_set) {
    PyErr_Print();
  }

StartDAQ();
StopDAQ();

  return 0; 
}

int BCP::ClearBusy() 
{
 if (debug_) Log("[BCP::ClearBusy] entering...", 3);
 if (debug_) Log("[BCP::ClearBusy] ...returning.", 3);
   
  return 0; 
}

int BCP::Clear() {
  
  
  return 0;
}

int BCP::BufferClear() {  //reset the buffers
  // For now clear our stored frames
  packedFrames_.clear();
  
  return 0;
}

int BCP::Print() { return 0; }

int BCP::Config(BoardConfig *bC) {
  //  Board::Config(bC);
  return 0; 

}


int BCP::Read(vector<WORD> &v) {
  Log("[BCP] Packing vector to send to event builder...", 1);

  /*int nempty = bp::extract<int>(bcpobjs_["BCP_Readout"].attr("ReadoutFIFO_Empty").attr("get")()); 

  while (nempty) {
    Log("[BCP] Waiting for frames to finish reading out...", 1);
    Readout(); 
    nempty = bp::extract<int>(bcpobjs_["BCP_Readout"].attr("ReadoutFIFO_Empty").attr("get")()); 
    
    }*/
  ProcessFrames(); 
  // Note, should probably reserve space in v 
  for (auto word : packedFrames_) {
    v.push_back(word);
    std::cout << v.size() << std::endl; 
    
  }

    
  return 0;
}
  


int BCP::StartDAQ()
{
  
    Log("[BCP::StartDAQ] entering...", 1);
    Log("[BCP::StartDAQ] ...returning.", 1);
    return 0;
}

int BCP::StopDAQ()
{
  Log("[BCP::StopDAQ] stopping...", 1);


    return 0;
}


void BCP::setupReadout() {

  // First clear busy
  try {
    bcpobjs_["Readout"].attr("NumReadoutWords").attr("set")(10); 
    bcpobjs_["TCDS"].attr("L1A_Select").attr("setDisp")("BCP Local");
    bcpobjs_["Readout"].attr("setTimeStampNow")();
    bcpobjs_["Readout"].attr("RstMon")();
    bcpobjs_["Readout"].attr("RstMon")();
    bcpobjs_["Readout"].attr("ClrBusy")();
    lpgbtfns_.attr("clearBusyRepeat")(bcp_);
    bcpobjs_["SwRx"].attr("resetMetrics")();
    // Replace fns
    bcpobjs_["SwRx"].attr("replaceProcess")(); 
    
    //bcpobjs_["TCDS"].attr("SWTrigger")();
    unsigned int FrameSize = 0xFFFFFFFF; 
    bcpobjs_["App"].attr("AppTx").attr("FrameSize").attr("set")(FrameSize, true);
  }
  catch (boost::python::error_already_set) {
    PyErr_Print();
  }
  
}

void BCP::Readout() {

  //  lpgbtfns_.attr("cacquireData")(bcp_); 
  lpgbtfns_.attr("cSendFrame")(bcp_); 

}

void BCP::ProcessFrames() {


  
  bp::object frames = bcpobjs_["SwRx"].attr("frames");
  int frmcnt = bp::extract<int>(bcpobjs_["builtins"].attr("len")(frames));

  if (debug_) 
    Log("[BCP::ProcessFrame] entering...", 1);

  std::cout << "Frame Count (Process): " << frmcnt << std::endl;


  std::map<unsigned long, std::vector< subframe > > store;
  std::set<unsigned long> lhcclocks; 

  
  for (int i = 0; i < frmcnt; i++) {
    bp::object frame = frames[i];
    int bytes = bp::extract<int>(frames[i].attr("getPayload")());
    int subFrameSz = 16*8;
    int subFrames = bytes/ subFrameSz;
    std::cout << "Sub Frames: " << subFrames << std::endl;
    ris::Frame &cfrm = bp::extract<ris::Frame&>(frame);
    std::cout << "Buffer Count:" << cfrm.bufferCount() << std::endl;
    std::cout << "Payload Size:" << cfrm.getPayload() << std::endl;

    uint64_t data = 0;
    uint64_t byte = 0; 
    int count = 0;

    ris::FrameIterator fit = cfrm.begin();
    
    //  ++fit;
    subframe sf;
	   
    
    uint64_t *arr = reinterpret_cast<uint64_t*>(fit.ptr());
    for (int i = 0; i<subFrames; i++) { 
      memcpy(&sf, arr, sizeof(subframe));
      fit += sizeof(subframe);
      arr = reinterpret_cast<uint64_t*>(fit.ptr());
      unsigned int dataBufferCount = sf.trailer >> 60;
      unsigned long timestamplhc = (sf.trailer & 0x0FFFFFFFFFFF0000) >> 16;
      store[timestamplhc].push_back(sf);
      lhcclocks.insert(timestamplhc); 

    }
  }
  
  std::vector<uint32_t> packedFrame = packFrame(store, lhcclocks);
  packedFrames_.insert(packedFrames_.end(), packedFrame.begin(), packedFrame.end()); 

  
}  
  

bool BCP::TriggerReceived()
{
  Log("[BCP] Trig recv'd...", 1);

  clock_gettime(CLOCK_MONOTONIC_RAW, &trig_time_);
  Readout(); 
  if (sw_trigger_)
    bcpobjs_["TCDS"].attr("SWTrigger")();

  return 1;
}

int BCP::SetBusyOn()
{

  // Enable LEMO Trigger, Input 0, Active High ___|---|___
  bcpobjs_["AppTx"].attr("ContinuousMode").attr("set")(0x1);
  //bcpobjs_["TCDS"].attr("LEMO_Config").attr("set")(0x1|0x10); 
  
  return 0;
}


int BCP::SetBusyOff()
{
  Log("Busy off ready to readout",1); 
  // Disable LEMO Trigger 
  //  bcpobjs_["TCDS"].attr("LEMO_Config").attr("set")(0x0); 
  bcpobjs_["AppTx"].attr("ContinuousMode").attr("set")(0x0);
  return 0;
}


int BCP::TriggerAck()
{
    return 0;
}



int BCP::SetTriggerStatus(TRG_t triggerType, TRG_STATUS_t triggerStatus)
{
    int status=0;
    if (triggerStatus == TRIG_ON) {
        status |= StopDAQ();
        status |= StartDAQ();
    } else if (triggerStatus == TRIG_OFF) {
        status |= StopDAQ();
    }
    return status;
}


std::vector<uint32_t> BCP::packFrame(  std::map<unsigned long, std::vector< subframe > > &store,
				    std::set<unsigned long> &lhcclocks )
{
  if (debug_) 
    Log("pack Frame",1); 
  ostringstream s; 
  
  std::vector<uint32_t> packedFrame; 
  for (auto clock : lhcclocks) {
    s << clock << std::endl; 
    s << store[clock][0].channelData[0] << std::endl; 
    Log(s.str(), 1); 
    uint32_t clkhigh = ((0xFFFFFFFF00000000&clock)>>32);
    uint32_t clklow = (0xFFFFFFFF & clock); 
    uint32_t separator = 0xF00F;
    packedFrame.push_back(clkhigh);
    packedFrame.push_back(clklow);
    for (int i = 0; i < 3; i++)
      packedFrame.push_back(separator);
	  
    std::vector<subframe> &f = store[clock];
    for (auto &sframe : f) {
      for (uint64_t ch : sframe.channelData) {
	uint32_t sample = ch&0xFFFFFFFF;
	packedFrame.push_back(sample); 
      }
    }
    packedFrame.push_back(separator);
  }
  return packedFrame; 



}

void BCP::enableSWTrig() { 
  sw_trigger_ = true; 
}
void BCP::disableSWTrig() { 
  sw_trigger_ = false; 
}

bool BCP::readoutBusy() { 

  int busy = bp::extract<int>(bcpobjs_["Readout"].attr("Readout_Busy").attr("get")());
  if(busy)
    return true; 
  return false; 

}

void BCP::readoutStatus() { 

  //  int busy = bp::extract<int>(bcpobjs_["Readout"].attr("Readout_Busy").attr("get")());
  //  std::cout << "(BCP) Readout Busy:" << busy << std::endl; 
  //int busy = bp::extract<int>(bcpobjs_["Readout"].attr("Readout_DataReady").attr("get")());
  //  std::cout << "(BCP) Data Ready:" << busy << std::endl; 
  int busy = bp::extract<int>(bcpobjs_["Readout"].attr("NumTriggersSeen").attr("get")());
  std::cout << "(BCP) Trigs Seen:" << busy << std::endl; 
  busy = bp::extract<int>(bcpobjs_["Readout"].attr("NumTriggersReadout").attr("get")());
  std::cout << "(BCP) Trigs Read:" << busy << std::endl; 


}

  /*
    Dead readout code 
    std::ofstream fout(fname, std::ios::binary | std::fstream::trunc); 

    std::time_t currTime = std::time(nullptr);

  for (auto clock : lhcclocks) {
    dfHeader header;
    header.clkhigh = ((0xFFFFFFFF00000000&clock)>>32);
    header.clklow = (0xFFFFFFFF & clock); 

    //    header.tsMSB = ((0xFFFFFFFF00000000&currTime)>>32); 
    //    header.tsLSB = (0xFFFFFFFF & currTime); 
    header.separator[0] = 0xF00F;
    header.separator[1] = 0xF00F;
    header.separator[2] = 0xF00F;

    fout.write(reinterpret_cast<char*>(&header), sizeof(header));
    
    std::vector<subframe> &f = store[clock];
    for (auto &sframe : f) {
      for (uint64_t ch : sframe.channelData) {
	uint32_t sample = ch&0xFFFFFFFF; 
	fout.write(reinterpret_cast<char*>(&sample), sizeof(sample));
      }
    }
    uint32_t trailer = 0xF00F;
    for (int i = 0; i < 3; i++) 
      fout.write(reinterpret_cast<char*>(&trailer), sizeof(trailer));
	    

    std::cout << std::hex << f[0].channelData[0] << std::endl; 
    std::cout << std::dec << clock << "," << f[0].trailer << std::endl; 
  }
  fout.close(); 

  }*/

#endif
