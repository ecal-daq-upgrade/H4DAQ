#ifndef NO_FITPIX
#include "interface/FITPIX.hpp"
#include "interface/Utility.hpp"

#include <cstdlib>
#include <iostream>
#include <sstream>
#include <bitset>

#include "TString.h"


//#define DEEPDEBUG 1
//#define BUSYDEBUG

int FITPIX::Init()
{
    Log("[FITPIX::Init] entering...", 1);    
    int rc=0;

    Log(Form("[FITPIX::Init] nDevices before Init %d",pxcGetDevicesCount()),1);

    if (pxcGetDevicesCount() == -1)
       rc |=pxcInitialize();

    _n_devices=pxcGetDevicesCount();
    rc |= pxcLoadDeviceConfiguration(_device_index,_chip_configFile.c_str());
    rc |= pxcSetThreshold(_device_index,_threshold);
    rc |= pxcGetTimepixClock(_device_index,&_clock_freq);

    //Common header for event header
    _header.clear();
    _header.push_back( ((int)(_frame_duration*1000000) && 0xFFFFF) | (((int)_threshold)<<20 ) | (_trigger_type<<30) | (_sparse_readout<<31)); // [ SR ] [ TT ] [ THL 29-20 ] [ FD 19-0 ] 
    _header.push_back( ((int)(_clock_freq*1000000) & 0x7FFFFFFF ) | (_tpx_mode<<31) ); // [ TPX MODE ] [ CL 30-0 ]

    if (_busyHandler_comPort_ != "")
      {
	_busyHandler_com = fopen(_busyHandler_comPort_.c_str(),"w");
	if (!_busyHandler_com)
	  rc |= 7;
      }

    if (rc)
	Log(Form("[FITPIX::Init]::[ERROR]::Error configuring device %d",_device_index), 1);    
    else
      Print();

#ifdef BUSYDEBUG
    if (_busyHandler_com)
      for (int i=0;i<100;++i)
	{
	  SetBusyOn();
	  usleep(1E5);
	  SetBusyOff();
	  usleep(1E5);
	}
#endif

    Log(Form("[FITPIX::Init] ...returning %d",rc), 1);

    return rc;
}


int FITPIX::Clear()
{
    if (_debug) Log("[FITPIX::Clear] entering...", 3);
    if (_debug) Log("[FITPIX::Clear] ...returning.", 3);
    return 0;
}


int FITPIX::BufferClear()
{
    if (_debug) Log("[FITPIX::BufferClear] entering..." ,3);
    if (_debug) Log("[FITPIX::BufferClear] ...returning.", 3);
    return 0;
}


int FITPIX::ClearBusy()
{  
    if (_debug) Log("[FITPIX::ClearBusy] entering...", 3);
    //int ret = BufferClear();
    int status = 0;
    status |= SetBusyOff();
    if (_debug) Log("[FITPIX::ClearBusy] ...returning.", 3);
    return status;
}


int FITPIX::Config(BoardConfig * bc)
{
    Log("[FITPIX::Config] entering...", 1);
    Board::Config(bc);
    ParseConfiguration(bc);
    Log("[FITPIX::Config] ...returning.", 1);
    return 0;
}

int FITPIX::SetEventHeader(std::vector<WORD> &v)
{
  v.assign(_header.begin(),_header.end());
  //create trig time wrt Utility::ref
  time_t ref=0;
  unsigned long x	= Utility::timestamp(&bef_trig_time_,&ref);
  unsigned long x_prime= x>>32;
  WORD x_msb = (( x_prime ) & 0xFFFFFFFF ); 
  WORD x_lsb = x & (0xFFFFFFFF);
  v.push_back(x_lsb); //time in sec
  v.push_back(x_msb); //time in usec

  x	= Utility::timestamp(&aft_trig_time_,&ref);
  x_prime= x>>32;
  x_msb = (( x_prime ) & 0xFFFFFFFF ); 
  x_lsb = x & (0xFFFFFFFF);
  v.push_back(x_lsb); //time in sec
  v.push_back(x_msb); //time in usec
}

int FITPIX::Read(std::vector<WORD> &v)
{
    if (_debug) Log("[FITPIX::Read] entering...", 3);
    SetEventHeader(v);
    if (_sparse_readout)
      {
	sparseReadout();
	for (int imem=0;imem<_mem_size;imem+=2)
	  {
	    //	    Log(Form("%d %d %d=>%X",imem,_mem_array[imem],_mem_array[imem+1],((_mem_array[imem+1]&0xFFFF)<<16)+(_mem_array[imem]&0xFFFF)),1);
	    v.push_back( ((_mem_array[imem+1]&0xFFFF)<<16)+(_mem_array[imem]&0xFFFF) );
	  }
      }
    else
      {
	for (int imem=0;imem<FITPIX_SINGLE_CHIP_PIXSIZE;imem+=2)
	  v.push_back( ((_frame_buff[imem+1]&0xFFFF)<<16)+ (_frame_buff[imem]&0xFFFF) );
      }
    //    v.push_back(_block[is]);
    if (_debug) Log("[FITPIX::Read] ...returning.", 3);
    return 0;
}


int FITPIX::ParseConfiguration(BoardConfig * bc)
{
    Log("[FITPIX::ParseConfiguration] entering...", 1);
    _chip_configFile         = bc->getElementContent("ChipConfig");
    _busyHandler_comPort_    = bc->getElementContent("BusyHandler");
    _device_index            = Configurator::GetInt(bc->getElementContent("DeviceIndex").c_str());
    _trigger_type            = Configurator::GetInt(bc->getElementContent("TriggerType").c_str());
    _tpx_mode                = Configurator::GetInt(bc->getElementContent("TpxMode").c_str());
    _threshold               = Configurator::GetDouble(bc->getElementContent("Threshold").c_str());
    _sparse_readout          = Configurator::GetInt(bc->getElementContent("SparseReadout").c_str());
    _frame_duration          = Configurator::GetDouble(bc->getElementContent("FrameDuration").c_str());
    _debug                   = Configurator::GetInt(bc->getElementContent("DebugLevel").c_str());
    return 0;
}


void FITPIX::sparseReadout()
{
    if (_debug) Log("[FITPIX::sparseReadout] entering...", 3);
    unsigned int j=1;
    _mem_size=0;
    _n_pixels=0;
    for (int i=0;i<_frame_size;i++)
    {
      if (_frame_buff[i]) 
      {
	_mem_array[2*(j)]=i;
	_mem_array[2*(j)+1]=_frame_buff[i];
	j++;
	_n_pixels++;
      }
    }
  _mem_size=j*2;
  _mem_array[0]=65535;
  _mem_array[1]=_mem_size;
  if (_debug) Log("[FITPIX::sparseReadout] ...returning.", 3);
}


int FITPIX::Print()
{
    if (_debug) Log("[FITPIX::Print] entering...", 3);
    double r_threshold;
    pxcGetThreshold(_device_index,&r_threshold);
    Log(Form("  **  Devices Connected      : %d", _n_devices), 1);
    Log(Form("  **  Device Index           : %d",_device_index), 1);
    Log(Form("  **  Chip configuration     : %s", _chip_configFile.c_str()), 1);
    Log(Form("  **  Trigger type           : %d", _trigger_type          ), 1);
    Log(Form("  **  Threshold              : %f", r_threshold), 1);
    Log(Form("  **  Frame Length (s)       : %f", _frame_duration     ), 1);
    Log(Form("  **  Debug level            : %d", _debug           ), 1);
    if (_busyHandler_comPort_ != "")
      Log(Form("  **  Busy Handler            : %s", _busyHandler_comPort_.c_str()  ), 1);

    char deviceName[FITPIX_ERRMSG_BUFF_SIZE];
    char chipId[FITPIX_ERRMSG_BUFF_SIZE];
    pxcGetDeviceName(_device_index,deviceName,FITPIX_ERRMSG_BUFF_SIZE);
    pxcGetDeviceChipID(_device_index,chipId,FITPIX_ERRMSG_BUFF_SIZE);
    Log(Form("  **  Device Name            : %s", deviceName), 1);
    Log(Form("  **  Chip   ID              : %s", chipId), 1);

    if (_debug) Log(Form("[FITPIX::Print] ...returning."), 3);
    return 0;
}


bool FITPIX::TriggerReceived()
{
  _frame_size=FITPIX_SINGLE_CHIP_PIXSIZE;

  
  clock_gettime(CLOCK_MONOTONIC_RAW, &bef_trig_time_);

  int rc = pxcMeasureSingleFrame(_device_index, _frame_duration, &_frame_buff[0],&_frame_size, _trigger_type);

  struct timespec acq_time;  //acq_time = frame_duration + readout time
  acq_time.tv_sec=floor(_frame_duration); //frame duration in second
  acq_time.tv_nsec=_frame_duration*1E9; //frame duration in ns
  acq_time.tv_nsec += 21010*1000; //expected time to read a frame ~21ms
  if (acq_time.tv_nsec >= 1000000000L)
  {
      acq_time.tv_sec++;
      acq_time.tv_nsec -= 1000000000L;
  }

  clock_gettime(CLOCK_MONOTONIC_RAW, &aft_trig_time_); //need to subtract frame duration and dead time
  Utility::timespecdiff(&aft_trig_time_,&acq_time,&trig_time_);

  if (rc!=0 || _frame_size != FITPIX_SINGLE_CHIP_PIXSIZE)
    Log(Form("[FITPIX::Read]::[ERROR]::Error reading device %d frame size %d status %d",_device_index,_frame_size,rc), 1);    

  return true;
}


int FITPIX::SetBusyOn()
{
  if (_busyHandler_com)
    fprintf(_busyHandler_com,"h\n");
  return 0;
}


int FITPIX::SetBusyOff()
{
  if (_busyHandler_com)
    fprintf(_busyHandler_com,"l\n");
  return 0;
}


int FITPIX::TriggerAck()
{
    return 0;
}


int FITPIX::SetTriggerStatus(TRG_t triggerType, TRG_STATUS_t triggerStatus)
{
   return 0;
}
#endif
