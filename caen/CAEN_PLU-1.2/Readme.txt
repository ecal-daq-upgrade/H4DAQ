-----------------------------------------------------------------------------

                   --- CAEN SpA - Computing Division ---

                                www.caen.it

  -----------------------------------------------------------------------------

  Program: CAEN_PLULib


  -----------------------------------------------------------------------------


  Content
  -----------------------------------------------------------------------------

  README.txt        :  This file.

  ReleaseNotes.txt  :  Revision History and notes.

   include      :  Include files
   lib			:  Library files
   bin			:  Executable files
   test         :  Test program files

  System Requirements
  -----------------------------------------------------------------------------
  - CAENComm Library
  - Linux



  How to get support
  -----------------------------------------------------------------------------
  Our Software Support Group is available for questions, support and any other
  software related issue concerning CAEN products; for software support
  visit the page www.caen.it/computing/support.php or send an email to
  support.computing@caen.it

